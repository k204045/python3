# python3


This DKRZ repository provides Python3 scripts and notebooks in the areas of 
Python language basics, data analysis, shapefile handle, and visualization that 
should make getting started programming with Python3 in the Earth Sciences a 
little more understandable. Some solutions to recurring visualization problems 
are explained in more detail.

## Content

<table style="width=100%"> 
    <colgroup>
      <col style="width:25%">
      <col style="width:75%">
    </colgroup>  
  
   <tr> <td><b> Basics </b></td><td></td></tr>
   <tr >
       <td  horizontal-align="left"> about functions <br></td>
       <td  horizontal-align="left"> [functions.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/basics/functions.ipynb) </td>
   </tr>
   
   <tr> <td><b> &nbsp; <br> Visualization </b></td><td></td></tr>
   <tr>
       <td> draw multiple plots in one figure </td>
       <td> [combine_multiple_plots_in_one_figure.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/combine_multiple_plots_in_one_figure.ipynb) </td>
   </tr>
   <tr>
       <td> de-rotate rotated grid data </td>
       <td> [derotate_curvilinear_grid_with_rotated_pole.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/derotate_curvilinear_grid_with_rotated_pole.ipynb) </td>
   </tr>
   <tr>
       <td> plot pre-projected data </td>
       <td> [pre-projected_data.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/pre-projected_data.ipynb) </td>
   </tr>
   <tr>
       <td> use an image as background map </td>
       <td> [map_background_images.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/map_background_images.ipynb) </td>
   </tr>
   <tr>
       <td> draw Ed Hawkin's warmin stripes </td>
       <td> [warming_stripes_plot_yearly_temp_avg.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/warming_stripes_plot_yearly_temp_avg.ipynb) </td>
   </tr>
   <tr>
       <td> create an animation and save it to an MPEG-4 file </td>
       <td> [animation_save_to_mpeg4.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/animation_save_to_mpeg4.ipynb) </td>
   </tr>
   <tr>
       <td> work with IMS daily Northern Hemisphere snow and ice analysis data <br> </td>
       <td> [IMS_snow_and_ice_chart_data.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/visualization/IMS_snow_and_ice_chart_data.ipynb) </td>
   </tr>
   
   <tr> <td><b> &nbsp; <br>  Shapefiles </b></td><td></td></tr>
   <tr>
       <td> Shapefile basics </td>
       <td> [shapefile_basics.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/shapefiles/shapefile_basics.ipynb) </td>
   </tr>
   <tr>
       <td> How to mask data using a shapefile </td>
       <td> [mask_data_using_shapefile_polygons.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/shapefiles/mask_data_using_shapefile_polygons.ipynb) <br>
            [shapefile_to_mask_data.ipynb](https://gitlab.dkrz.de/k204045/python3/-/blob/main/notebooks/shapefiles/shapefile_to_mask_data.ipynb) </td>
   </tr>
   
</table>


<br>

----

## Used Python packages

<table><col style="width: 20%;">
   <tr><th style="text-align: left;"> Packages </th>
       <th  style="text-align: left;"> URL </th>
   </tr>
   <tr>
      <td> NumPy </td>
      <td> [https://numpy.org/](https://numpy.org/) </td>
   </tr>
   <tr>
      <td> Xarray </td>
      <td> [https://docs.xarray.dev/en/stable/](https://docs.xarray.dev/en/stable/)</td>
   </tr>
   <tr>
      <td> SciPy </td>
      <td> [https://scipy.org/](https://scipy.org/)</td>
   </tr>
   <tr>
      <td> Pandas </td>
      <td> [https://pandas.pydata.org/](https://pandas.pydata.org/)</td>
   </tr>
   <tr>
      <td> GeoPandas </td>
      <td> [https://geopandas.org/en/stable/](https://geopandas.org/en/stable/)</td>
   </tr>
   <tr>
      <td> Regionmask </td>
      <td> [https://regionmask.readthedocs.io/en/stable/](https://regionmask.readthedocs.io/en/stable/)</td>
   </tr>
   <tr>
      <td> CDO </td>
      <td> [https://code.mpimet.mpg.de/projects/cdo](https://code.mpimet.mpg.de/projects/cdo)</td>
   </tr>
   <tr>
      <td> Datashader </td>
      <td> [https://datashader.org/](https://datashader.org/)</td>
   </tr>
   <tr>
      <td> Matplotlib </td>
      <td> [https://matplotlib.org/](https://matplotlib.org/)</td>
   </tr>
   <tr>
      <td> Cartopy </td>
      <td> [https://scitools.org.uk/cartopy/docs/latest/index.html](https://scitools.org.uk/cartopy/docs/latest/index.html)</td>
   </tr>
   <tr>
      <td> CMOCEAN </td>
      <td> [https://github.com/matplotlib/cmocean](https://github.com/matplotlib/cmocean)</td>
   </tr>
</table>
    
<br>

----

## License

<b>Copyright 2020-2023 Deutsches Klimarechenzentrum GmbH (DKRZ) </b>

<b>Licensed under CC-BY-NC-SA-4.0 </b>

<table>
   <tr><th colspan="2"  style="text-align: left;"> You are free to: </th></tr>
   <tr>
      <td>  <em>Share</em> </td>
      <td>  copy and redistribute the material in any medium or format </td>
  </tr>
  <tr>
      <td>  <em> Adapt </em> </td>
      <td> remix, transform, and build upon the material</td>
  </tr>
</table>

The licensor cannot revoke these freedoms as long as you follow the license terms.

<table>
   <tr><th colspan="2"  style="text-align: left;"> Under the following terms: </th></tr>
   <tr>
      <td>  <em> Attribution </em> </td>
      <td>  You must give appropriate credit, provide a link to the license, and 
            indicate if changes were made. You may do so in any reasonable manner, 
            but not in any way that suggests the licensor endorses you or your use. </td>
  </tr>
  <tr>
      <td>  <em> NonCommercial </em> </td>
      <td>  You may not use the material for commercial purposes. </td>
  </tr>
  <tr>
      <td>  <em> ShareAlike </em> </td>
      <td>  If you remix, transform, or build upon the material, you must 
            distribute your contributions under the same license as the original. </td>
  </tr>
  <tr>
      <td>  <em> No additional restrictions </em> </td>
      <td>  You may not apply legal terms or technological measures that legally 
            restrict others from doing anything the license permits. </td>
  </tr>
</table>

